﻿using System;

namespace Cryptography
{
    public static class CaesarCipher
    {
        private static readonly string _alphabet = Alphabet.PolishAlphabet;

        public static string Encode(string text, int shift)
        {
            if (shift < 0)
                throw new ArgumentOutOfRangeException("The shift must be greater than 0.", shift.ToString());

            var encodedText = "";

            for (int i = 0; i < text.Length; i++)
            {
                if (_alphabet.IndexOf(text[i]) < 0)
                    throw new ArgumentOutOfRangeException("Alphabet does not contain selected letter.", text[i].ToString());

                var index = _alphabet.IndexOf(text[i]) + shift;
                encodedText += _alphabet[index % _alphabet.Length];
            }
            return encodedText;
        }

        public static string Decode(string encodedText, int shift)
        {
            if (shift < 0)
                throw new ArgumentOutOfRangeException("The shift must be greater than 0.", shift.ToString());

            var decodedText = "";

            for (int i = 0; i < encodedText.Length; i++)
            {
                if (_alphabet.IndexOf(encodedText[i]) < 0)
                    throw new ArgumentOutOfRangeException("Alphabet does not contain selected letter.", encodedText[i].ToString());

                var index = _alphabet.IndexOf(encodedText[i]) - shift % _alphabet.Length;
                var calcIndex = (index > 0) ? index % _alphabet.Length : _alphabet.Length + index;
                decodedText += _alphabet[(calcIndex)];
            }
            return decodedText;
        }
    }
}
