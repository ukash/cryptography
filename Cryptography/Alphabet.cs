﻿namespace Cryptography
{
    internal static class Alphabet
    {
        public static string PolishAlphabet = "AĄBCĆDEĘFGHIJKLŁMNŃOÓPQRSŚTUWXYZŻŹaąbcćdeęfghijklłmnńoópqrsśtuwxyzżź0123456789 !,.";
        public static string PlayfairAlphabet = "ABCDEFGHIKLMNOPQRSTUVWXYZ"; // J is missing
    }
}
