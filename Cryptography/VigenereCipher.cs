﻿using System;

namespace Cryptography
{
    public static class VigenereCipher
    {
        private static readonly string _alphabet = Alphabet.PolishAlphabet;
        private static readonly string[] _alphabets;

        static VigenereCipher()
        {
            _alphabets = new string[_alphabet.Length];

            for (int i = 0; i < _alphabet.Length; i++)
                _alphabets[i] = _alphabet.Substring(i) + _alphabet.Substring(0, i);
        }

        public static string Encode(string text, string key)
        {

            if(string.IsNullOrEmpty(key))
                throw new ArgumentNullException("You must specify a key.");

            var encodedText = "";

            foreach (var k in key)
            {
                if (_alphabet.IndexOf(k) < 0)
                    throw new ArgumentOutOfRangeException("Alphabet does not contain selected letter.", k.ToString());
            }

            for (int i = 0; i < text.Length; i++)
            {
                if (_alphabet.IndexOf(text[i]) < 0)
                    throw new ArgumentOutOfRangeException("Alphabet does not contain selected letter.", text[i].ToString());

                var columnIndex = _alphabet.IndexOf(text[i]);
                var rowIndex = _alphabet.IndexOf(key[i % key.Length]);

                encodedText += _alphabets[columnIndex][rowIndex];
            }
            return encodedText;
        }

        public static string Decode(string encodedText, string key)
        {
            if (string.IsNullOrEmpty(key))
                throw new ArgumentNullException("You must specify a key.");

            var reversedKey = "";

            for (int i = 0; i < key.Length; i++)
            {
                if (_alphabet.IndexOf(key[i]) < 0)
                    throw new ArgumentOutOfRangeException("Alphabet does not contain selected letter.", key[i].ToString());

                var index = (_alphabet.Length - _alphabet.IndexOf(key[i])) % _alphabet.Length;

                reversedKey += _alphabet[index];
            }
            return Encode(encodedText, reversedKey);
        }
    }
}
