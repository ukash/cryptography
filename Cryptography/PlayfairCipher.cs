﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Cryptography
{
    public static class PlayfairCipher
    {
        private static readonly string _alphabet = Alphabet.PlayfairAlphabet;
        private static string[] _alphabets;

        public static string Encode(string text, string key)
        {
            if (string.IsNullOrEmpty(key))
                throw new ArgumentNullException("You must specify a key.");

            key = key.Replace(" ", "").ToUpper().Replace("J", "I");

            foreach (var k in key)
            {
                if (_alphabet.IndexOf(k) < 0)
                    throw new ArgumentOutOfRangeException("Alphabet does not contain selected letter.", k.ToString());
            }

            text = text.Replace(" ", "").ToUpper().Replace("J", "I");

            for (int i = 0; i < text.Length; i++)
            {
                if (_alphabet.IndexOf(text[i]) < 0)
                    throw new ArgumentOutOfRangeException("Alphabet does not contain selected letter.", text[i].ToString());
            }

            _alphabets = generateKeyTable(key);

            var rotatedAlphabets = new string[5];

            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    rotatedAlphabets[i] += _alphabets[j][i];
                }
            }

            var diagrams = new List<string>();

            for (int i = 0; i < text.Length; i += 2)
            {
                if (i == text.Length - 1)
                {
                    diagrams.Add(text[i] + "X");
                }

                else if (text[i] != text[i + 1])
                {
                    diagrams.Add(text.Substring(i, 2));
                }
                else
                {
                    diagrams.Add(text[i] + "X");
                    i--;
                }
            }

            var encodedText = "";

            foreach (var d in diagrams)
            {
                var result = CodeDiagram(d, _alphabets);

                if (string.IsNullOrEmpty(result))
                    result = CodeDiagram(d, rotatedAlphabets);

                if (string.IsNullOrEmpty(result))
                {
                    var firstIndex = -1;
                    var firstAlphabet = "";

                    foreach (var a in _alphabets)
                    {
                        firstIndex = a.IndexOf(d[0]);

                        if (firstIndex != -1)
                        {
                            firstAlphabet = string.Copy(a);
                            break;
                        }
                    }

                    foreach (var a in _alphabets)
                    {
                        var secondIndex = a.IndexOf(d[1]);

                        if (secondIndex != -1)
                        {
                            result += firstAlphabet[secondIndex] + a[firstIndex].ToString();
                            break;
                        }
                    }
                }
                encodedText += result;
            }
            return encodedText;
        }

        public static string Decode(string encodedText, string key)
        {
            if (string.IsNullOrEmpty(key))
                throw new ArgumentNullException("You must specify a key.");

            key = key.Replace(" ", "").ToUpper().Replace("J", "I");

            foreach (var k in key)
            {
                if (_alphabet.IndexOf(k) < 0)
                    throw new ArgumentOutOfRangeException("Alphabet does not contain selected letter.", k.ToString());
            }

            encodedText = encodedText.Replace(" ", "").ToUpper().Replace("J", "I");

            for (int i = 0; i < encodedText.Length; i++)
            {
                if (_alphabet.IndexOf(encodedText[i]) < 0)
                    throw new ArgumentOutOfRangeException("Alphabet does not contain selected letter.", encodedText[i].ToString());
            }

            _alphabets = generateKeyTable(key);

            var rotatedAlphabets = new string[5];

            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    rotatedAlphabets[i] += _alphabets[j][i];
                }
            }
            var diagrams = new List<string>();

            for (int i = 0; i < encodedText.Length / 2; i++)
            {
                diagrams.Add(encodedText.Substring(i * 2, 2));
            }

            var decodedText = "";

            foreach (var d in diagrams)
            {
                var result = DecodeDiagram(d, _alphabets);

                if (string.IsNullOrEmpty(result))
                    result = DecodeDiagram(d, rotatedAlphabets);

                if (string.IsNullOrEmpty(result))
                {
                    var firstIndex = -1;
                    var firstAlphabet = "";

                    foreach (var a in _alphabets)
                    {
                        firstIndex = a.IndexOf(d[0]);

                        if (firstIndex != -1)
                        {
                            firstAlphabet = string.Copy(a);
                            break;
                        }
                    }

                    foreach (var a in _alphabets)
                    {
                        var secondIndex = a.IndexOf(d[1]);

                        if (secondIndex != -1)
                        {
                            result += firstAlphabet[secondIndex] + a[firstIndex].ToString();
                            break;
                        }
                    }
                }
                decodedText += result;
            }
            return decodedText;
        }

        private static string CodeDiagram(string diagram, string[] alphabets)
        {
            var encodedText = "";

            foreach (var a in alphabets)
            {
                if (a.Contains(diagram[0]) && a.Contains(diagram[1]))
                {
                    var firstIndex = a.IndexOf(diagram[0]);
                    encodedText += a[(firstIndex + 1) % a.Length];

                    var secondIndex = a.IndexOf(diagram[1]);
                    encodedText += a[(secondIndex + 1) % a.Length];
                    return encodedText;
                }
            }
            return "";
        }

        private static string DecodeDiagram(string diagram, string[] alphabets)
        {
            var decodedText = "";

            foreach (var a in alphabets)
            {
                if (a.Contains(diagram[0]) && a.Contains(diagram[1]))
                {
                    var firstIndex = a.IndexOf(diagram[0]);
                    decodedText += a[(firstIndex - 1 < 0) ? a.Length - 1 : firstIndex - 1];

                    var secondIndex = a.IndexOf(diagram[1]);
                    decodedText += a[(secondIndex - 1 < 0) ? a.Length - 1 : secondIndex - 1];
                    return decodedText;
                }
            }
            return "";
        }

        private static string[] generateKeyTable(string key)
        {
            var keyTable = new string[5];

            var keyword = new string(key.Replace(" ", "").ToUpper().Replace("J", "I").Distinct().ToArray());

            var alphabetCopy = string.Copy(_alphabet);

            foreach (var k in keyword)
            {
                var index = alphabetCopy.IndexOf(k);
                alphabetCopy = alphabetCopy.Remove(index, 1);
            }

            var alphabet = keyword + alphabetCopy;

            var alphabets = new string[5];

            for (int i = 0; i < 5; i++)
            {
                alphabets[i] = alphabet.Substring(i * 5, 5);
            }
            return alphabets;
        }
    }
}
